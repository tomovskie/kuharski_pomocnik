��          �                    H   !     j     o       	   �     �     �     �     �     �     �     �  \        d  Q  p     �  U   �       
   $     /     @     P     h     y     �     �     �     �  =   �        By  Copyright &copy; [current_year] [site_title] | Powered by [theme_author] Home Leave a Comment Next %s Next Page Post Comment &raquo; Previous %s Previous Page Read More &raquo; Search Search Results for: %s Search results for: %s Sorry, but nothing matched your search terms. Please try again with some different keywords. Type here.. Project-Id-Version: Astra 2.3.4
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/astra
POT-Creation-Date: 2020-03-17 07:32:09+00:00
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
PO-Revision-Date: 2020-04-20 18:13+0000
Last-Translator: Tjasa <ice.tjasa@gmail.com>
Language-Team: Slovenščina
Language: sl_SI
Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3||n%100==4 ? 2 : 3;
X-Poedit-Country: United States
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_html__;esc_attr_e;esc_html_e;esc_attr_x:1,2c;esc_html_x:1,2c;
X-Poedit-Basepath: ../
X-Poedit-SearchPath-0: .
X-Poedit-Bookmarks: 
X-Textdomain-Support: yes
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.3.2 Avtor Vse pravice pridržane &copy; [current_year] [site_title] | Powered by [theme_author] Domov Komentiraj Naslednji recept Naslednja stran Objavi komentar &raquo; Prejšnji recept Prejšnja stran Preberi več &raquo; Išči Rezultati iskanja za %s Rezultati iskanja za %s Vaše iskanje nima zadetkov. Poskusite z drugimi sestavinami. Piši tukaj 