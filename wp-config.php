<?php

/**

 * The base configuration for WordPress

 *

 * The wp-config.php creation script uses this file during the

 * installation. You don't have to use the web site, you can

 * copy this file to "wp-config.php" and fill in the values.

 *

 * This file contains the following configurations:

 *

 * * MySQL settings

 * * Secret keys

 * * Database table prefix

 * * ABSPATH

 *

 * @link https://codex.wordpress.org/Editing_wp-config.php

 *

 * @package WordPress

 */


// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

define( 'DB_NAME', '' );


/** MySQL database username */

define( 'DB_USER', '' );


/** MySQL database password */

define( 'DB_PASSWORD', '' );


/** MySQL hostname */

define( 'DB_HOST', '' );


/** Database Charset to use in creating database tables. */

define( 'DB_CHARSET', 'utf8mb4' );


/** The Database Collate type. Don't change this if in doubt. */

define( 'DB_COLLATE', '' );


/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define( 'AUTH_KEY',         ']CeNYQ#5q!BQdAy.Xt)V16<elaEEs#{=#Jq}tpX}N]nyaUjsZ3pRocq7!Z~~E%<&' );

define( 'SECURE_AUTH_KEY',  'U/EU&DMX$^(++`dI]+M62Eoth,e Y`L;^|u0eWb8p9amQNz{xB8wf9E$Lh+5HeJ)' );

define( 'LOGGED_IN_KEY',    'Ui F`+-|+9t~Pg[uqffPQg6$Rf[}744ZXMFjf0Z93ws6ZdZyd7eHh:=`T{hxT-tI' );

define( 'NONCE_KEY',        'AQ^wK!qX8._+kIft66#RN/k8-WyLI[xc@#+f9rG@]+|Fl70?o,AkJxULN1.+)+$[' );

define( 'AUTH_SALT',        '!Hp6~,an_1+Rt2g[-~S@W|US? A5Tbz$wT%UC=Kb|iqv!.K(<SzQnnBj(!j))Q#O' );

define( 'SECURE_AUTH_SALT', 'Nu_dQ08Lq&Qcx$%i$}6 CP[oyQ1K)qvYt{mPU~%a#;psQH3*}=ZPB-i&55MZClsx' );

define( 'LOGGED_IN_SALT',   '`htv]E M^M.V~Vu?YNXn)pI-Wf|>OYa{>0u7UsNvyBjAwee)rp}5HbnFr%/$QsYx' );

define( 'NONCE_SALT',       'yy=BK;F$*,seQt^LbYc<qz^(KF`)FWFgH zKP953yFJ[,xD=zxG!XAPmsNSY>h9p' );


/**#@-*/


/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each

 * a unique prefix. Only numbers, letters, and underscores please!

 */

$table_prefix = 'wp_trg775';


/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 *

 * For information on other constants that can be used for debugging,

 * visit the Codex.

 *

 * @link https://codex.wordpress.org/Debugging_in_WordPress

 */

define( 'WP_DEBUG', false );


/* That's all, stop editing! Happy publishing. */


/** Absolute path to the WordPress directory. */

if ( ! defined( 'ABSPATH' ) ) {

	define( 'ABSPATH', dirname( __FILE__ ) . '/' );

}


/** Sets up WordPress vars and included files. */

require_once( ABSPATH . 'wp-settings.php' );

